## 1. Lombok 插件编译时提示找不到符号

多一般是因为 `IDEA` 默认创建的 `Gradle` 项目设置的 `Build Tool` 不是 `IDEA` 导致 `Lombok` 插件无法生效。如下图所示修改即可：
![](./images/build-runner.png)
