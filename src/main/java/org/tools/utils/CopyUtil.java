package org.tools.utils;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.core.type.TypeReference;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CopyUtil {

    /**
     * 性能差，深拷贝稳定
     *
     * @param origin 原始数据
     * @param clazz  目标数据类型
     * @param <T>    目标数据类型
     * @param <O>    原始数据类型
     * @return 目标数据
     */
    public static <T, O> List<T> copy(Collection<O> origin, Class<T> clazz) {
        if (origin == null) return new ArrayList<>();
        return JSON.parseArray(JSON.toJSONString(origin), clazz);
    }

    /**
     * 深拷贝稳定
     *
     * @param origin 原始数据
     * @param clazz  目标数据类型
     * @param <T>    目标数据类型
     * @param <O>    原始数据类型
     * @return 目标数据
     */
    public static <T, O> T copy(O origin, Class<T> clazz) {
        if (origin == null) {
            return null;
        }
        return JSON.parseObject(JSON.toJSONString(origin), clazz);
    }

    public static <T> List<T> copyList(List<T> origin) {
        if (CollectionUtils.isEmpty(origin)) {
            return new ArrayList<>();
        }
        String json = JsonUtils.toJson(origin);
        return JsonUtils.fromJson(json, new TypeReference<List<T>>() {
        });
    }
}
