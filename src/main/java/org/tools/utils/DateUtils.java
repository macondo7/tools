package org.tools.utils;


import org.tools.enums.TimezoneEnum;

import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class DateUtils {
    public static String getTodayStringInBeijing() {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
        return simpleDateFormat.format(date);
    }

    public static String getLastDayStringInBeijing() {
        Date date = new Date(new Date().getTime() - 24 * 3600 * 1000L);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
        return simpleDateFormat.format(date);
    }

    public static String getNextDayStringInBeijing() {
        Date date = new Date(new Date().getTime() + 24 * 3600 * 1000L);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
        return simpleDateFormat.format(date);
    }

    /**
     * 获取指定日期的上一天日期，参数格式 yyyyMMdd
     */
    public static String getLastDayString(String dateStr) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
            Date date = simpleDateFormat.parse(dateStr);
            date = new Date(date.getTime() - 24 * 3600 * 1000L);
            return simpleDateFormat.format(date);
        } catch (Exception e) {
            throw new RuntimeException("日期格式错误");
        }
    }

    /**
     * 获取指定日期的下一天日期，参数格式 yyyyMMdd
     */
    public static String getNextDayString(String dateStr) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
            Date date = simpleDateFormat.parse(dateStr);
            date = new Date(date.getTime() + 24 * 3600 * 1000L);
            return simpleDateFormat.format(date);
        } catch (Exception e) {
            throw new RuntimeException("日期格式错误");
        }
    }

    /**
     * 获取北京时间起始范围
     * 1.当前时间<16点，取前一天16点~今天16点
     * 2.当前时间>16点，取今天16点~明天16点
     */
    public static List<Date> getBeijingTimePair() {
        List<Date> result = new ArrayList<>();
        Calendar today16 = Calendar.getInstance();
        today16.set(Calendar.HOUR_OF_DAY, 16);
        today16.set(Calendar.MINUTE, 0);
        today16.set(Calendar.SECOND, 0);
        today16.set(Calendar.MILLISECOND, 0);
        long today16Mill = today16.getTimeInMillis();
        long currentMill = System.currentTimeMillis();
        Date begin = new Date();
        Date end = new Date();
        if (today16Mill > currentMill) {
            //-16~16
            begin.setTime(today16Mill - 24 * 3600 * 1000);
            end.setTime(today16Mill);
        } else {
            begin.setTime(today16Mill);
            end.setTime(today16Mill + 24 * 3600 * 1000);
        }
        result.add(begin);
        result.add(end);
        return result;
    }

    /**
     * 获取指定日期, yyyyMMdd格式的纽约时间的起止时间戳
     *
     * @param dateStr yyyyMMdd
     * @return List<Long>
     */
    public static List<Long> getNewYorkDayBeginEndTime(String dateStr) {
        List<Long> result = new ArrayList<>();
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone(TimezoneEnum.US.getTimezoneId()));
            Date date = simpleDateFormat.parse(dateStr);
            long timeMills = date.getTime();
            result.add(timeMills);
            result.add(timeMills + 24 * 3600 * 1000L);
            return result;
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }

    public static Long getMilliSecondsDayBegin(String timezoneId) {
        ZonedDateTime zonedDateTime = ZonedDateTime.now(ZoneId.of(timezoneId));
        ZonedDateTime startTime = zonedDateTime.with(LocalTime.MIN);
        return startTime.toInstant().toEpochMilli();
    }

    public static Long getMilliSecondsDayEnd(String timezoneId) {
        ZonedDateTime zonedDateTime = ZonedDateTime.now(ZoneId.of(timezoneId));
        ZonedDateTime startTime = zonedDateTime.with(LocalTime.MAX);
        return startTime.toInstant().toEpochMilli();
    }

    /**
     * 获取指定时区的日期和小时的时间戳
     *
     * @param date     日期，格式yyyyMMdd，如20190101
     * @param hour     小时，0~23
     * @param timezone 时区
     */
    public static Long getTimestamp(int date, int hour, TimezoneEnum timezone) {
        int year = date / 10000;
        int month = (date % 10000) / 100;
        int day = date % 100;
        // 获取指定时区的时间戳
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month - 1, day, hour, 0, 0);
        calendar.setTimeZone(TimeZone.getTimeZone(timezone.getTimezoneId()));
        return calendar.getTimeInMillis();
    }

    /**
     * 获取指定时区的日期和小时的时间戳(日期格式为yyyy-MM-dd)
     *
     * @param date     日期，格式yyyyMMdd，如2019-01-01
     * @param hour     小时，0~23
     * @param timezone 时区
     */
    public static Long getTimestamp(String date, int hour, TimezoneEnum timezone) {
        String[] dateArr = date.split("-");
        int year = Integer.parseInt(dateArr[0]);
        int month = Integer.parseInt(dateArr[1]);
        int day = Integer.parseInt(dateArr[2]);
        // 获取指定时区的时间戳
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month - 1, day, hour, 0, 0);
        calendar.setTimeZone(TimeZone.getTimeZone(timezone.getTimezoneId()));
        return calendar.getTimeInMillis();
    }

    /**
     * 获取北京时间指定日期和小时的时间戳
     */
    public static Long getBeijingTimestamp(int date, int hour) {
        return getTimestamp(date, hour, TimezoneEnum.CN);
    }

    /**
     * duration 转换为 2 分 3 秒的字符串格式, duration 单位为毫秒
     */
    public static String getDurationStr(long duration) {
        duration = duration / 1000;
        String durationStr = "";
        if (duration < 60) {
            durationStr = String.format("%d秒", duration);
        } else if (duration % 60 == 0) {
            durationStr = String.format("%d分钟", duration / 60);
        } else {
            durationStr = String.format("%d分%d秒", duration / 60, duration % 60);
        }
        return durationStr;
    }
}
