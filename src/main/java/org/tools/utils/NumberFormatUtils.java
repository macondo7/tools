package org.tools.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class NumberFormatUtils {
    public static Double oneLittleDouble(Double a) {
        BigDecimal b = new BigDecimal(a);
        return b.setScale(1, RoundingMode.HALF_UP).doubleValue();
    }

    public static Double twoLittleDouble(Double a) {
        BigDecimal d = new BigDecimal(a);
        return d.setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    public static void main(String[] args) {
        System.out.println(oneLittleDouble(1.234));
        System.out.println(twoLittleDouble(1.235));
    }
}
