package org.tools.vo;

/**
 * Definition for singly-linked list.
 */
public class ListNode {
    public int val;
    public ListNode next;

    public ListNode() {
    }

    public ListNode(int val) {
        this.val = val;
    }

    public ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }

    public void println() {
        ListNode cur = this;
        while (cur != null) {
            if (cur.next != null) {
                System.out.print(cur.val + " -> ");
            } else {
                System.out.print(cur.val);
            }
            cur = cur.next;
        }
        System.out.println();
    }
}
