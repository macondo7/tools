package org.tools.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class CommMark implements Serializable {
    private Long csId;
}
