package org.tools.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class NewMark implements Serializable {
    private String csId;

    private String csName;
}
