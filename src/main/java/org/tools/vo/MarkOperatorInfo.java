package org.tools.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MarkOperatorInfo implements Serializable {
    private Long markOperatorId;

    private String markOperatorName;

    private Long markTimestamp;
}
