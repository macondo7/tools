package org.tools.datastructure;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class AutomaticType {
    public static void main(String[] args) {
        // docs: Executors wrapped ThreadPoolExecutor
        // future: directly use java.util.concurrent.CompletableFuture
        ExecutorService executorService = Executors.newFixedThreadPool(5);

        AtomicInteger steep = new AtomicInteger(0);
        List<Callable<Void>> tasks = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            tasks.add(() -> {
                steep.addAndGet(4);
                return null;
            });
        }
        try {
            executorService.invokeAll(tasks);
        } catch (InterruptedException e) {
            log.error("error", e);
        }
        log.info("steep: {}", steep.get());
        executorService.shutdown();
    }
}
