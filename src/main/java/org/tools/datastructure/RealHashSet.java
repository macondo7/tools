package org.tools.datastructure;

import java.util.HashSet;
import java.util.Set;

public class RealHashSet {
    public static void main(String[] args) {
        Set<Long> storeUidSet = new HashSet<>();
        storeUidSet.add(1L);
        storeUidSet.add(2L);
        storeUidSet.add(3L);
        storeUidSet.add(4L);

        // test: HashSet iterator and remove method
        Long mockUid = null;
        while (!storeUidSet.isEmpty()) {
            mockUid = storeUidSet.iterator().next();
            storeUidSet.remove(mockUid);
            if (mockUid > 2) {
                break;
            }
        }
        System.out.println(mockUid); // 3
        System.out.println(storeUidSet); // [4]
    }
}
