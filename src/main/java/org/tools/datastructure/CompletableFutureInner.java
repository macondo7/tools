package org.tools.datastructure;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class CompletableFutureInner {
    private static final ExecutorService EXECUTOR = Executors.newFixedThreadPool(5);

    private static final ThreadPoolExecutor THREAD_POOL_EXECUTOR = new ThreadPoolExecutor(7, 7,
            0L, TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<>());

    public static void main(String[] args) {
        // test: CompletableFuture runner beginner
        test_completableFuture();
        log.info("=====================================");
        // test: CompletableFuture runner with thread pool executor
        test_completableFuture_with_thread_pool_executor();
        // shutdown
        EXECUTOR.shutdown();
        THREAD_POOL_EXECUTOR.shutdown();
    }

    private static void test_completableFuture() {
        CompletableFuture<?>[] finishFuture = new CompletableFuture[10];
        AtomicInteger steep = new AtomicInteger(0);

        Runnable o = () -> steep.addAndGet(4);
        for (int i = 0; i < 10; i++) {
            finishFuture[i] = CompletableFuture.runAsync(o, EXECUTOR);
        }
        CompletableFuture.allOf(finishFuture).join();
        log.info("test_completableFuture: {}", steep.get());
    }

    private static void test_completableFuture_with_thread_pool_executor() {
        CompletableFuture<?>[] finishFuture = new CompletableFuture[200];
        AtomicInteger steep = new AtomicInteger(0);
        Runnable o = () -> steep.addAndGet(11);
        for (int i = 0; i < 200; i++) {
            finishFuture[i] = CompletableFuture.runAsync(o, THREAD_POOL_EXECUTOR);
        }
        CompletableFuture.allOf(finishFuture).join();
        log.info("test_completableFuture_with_thread_pool_executor: {}", steep.get());
    }
}
