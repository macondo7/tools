package org.tools.example;

import com.google.common.collect.Lists;
import org.tools.utils.CopyUtil;

import java.util.ArrayList;
import java.util.List;

public class DeepCopyExample {
    public static void main(String[] args) {
        List<Integer> originalList = new ArrayList<>();
        originalList.add(1);
        originalList.add(2);
        originalList.add(3);

        List<Integer> copiedList = CopyUtil.copyList(originalList);
        // print
        System.out.println("originalList: " + originalList);
        System.out.println("copiedList: " + copiedList);

        originalList.addAll(Lists.newArrayList(4, 5, 6));
        // print
        System.out.println("originalList: " + originalList);
        System.out.println("copiedList: " + copiedList);
    }
}
