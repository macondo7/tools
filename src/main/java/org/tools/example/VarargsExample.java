package org.tools.example;

import lombok.extern.slf4j.Slf4j;

import java.net.URI;
import java.net.URISyntaxException;

@Slf4j
public class VarargsExample {
    public static void test(String... args) {
        for (String arg : args) {//当作数组用foreach遍历
            System.out.println(arg);
        }
    }

    public static void main(String[] args) {
        test("a", "b", "c", null);
        try {
            URI baseUri = new URI("https://www.baidu.com//");
            URI relativeUri = new URI("/goods.html?goods_id=601099512453687");
            URI resolvedUri = baseUri.resolve(relativeUri);
            System.out.println(resolvedUri);
        } catch (URISyntaxException e) {
            log.error("fillGoodsInfo resolve couponLandingPageUrl error:", e);
        }
    }
}