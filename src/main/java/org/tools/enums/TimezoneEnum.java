package org.tools.enums;

import lombok.Getter;

@Getter
public enum TimezoneEnum {
    CN("China", "Asia/Shanghai"),

    CA("Canada", "America/New_York"),

    JP("Japan", "Asia/Tokyo"),

    US("United States", "America/New_York"),

    SA("Saudi Arabia", "Asia/Riyadh"),
    ;

    private final String region;

    private final String timezoneId;

    TimezoneEnum(String region, String timezoneId) {
        this.region = region;
        this.timezoneId = timezoneId;
    }
}
