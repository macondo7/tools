package org.tools.enums;


import lombok.Getter;

@Getter
public enum CsStatusEnum {
    CS_ONLINE(1, "在线"),

    CS_BUSY(2, "忙碌"),

    CS_OFFLINE(3, "离线"),

    CS_DISCONNECT(4, "下线"),

    CS_REST(5, "小休"),

    CS_DINNER(6, "就餐"),

    CS_TRAIN(7, "培训"),

    CS_TEACH(8, "教学");

    private final int code;
    private final String desc;

    CsStatusEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static CsStatusEnum of(int code) {
        for (CsStatusEnum csStatusEnum : CsStatusEnum.values()) {
            if (csStatusEnum.getCode() == code) {
                return csStatusEnum;
            }
        }
        return null;
    }

    public static CsStatusEnum of(String codeStr) {
        int code;
        try {
            code = Integer.parseInt(codeStr);
        } catch (NumberFormatException e) {
            return null;
        }
        return of(code);
    }
}
