package org.tools.tests;

import org.tools.enums.CsStatusEnum;

public class SwitchUtil {
    public static void main(String[] args) {
        CsStatusEnum csStatus = CsStatusEnum.of("2");
        if (csStatus == null) {
            System.out.println("Invalid code");
            return;
        }
        switch (csStatus) {
            case CS_ONLINE:
                System.out.println("Online");
            case CS_BUSY:
                System.out.println("Busy");
            case CS_OFFLINE:
                System.out.println("Offline");
            case CS_DISCONNECT:
                System.out.println("Disconnect");
            case CS_REST:
                System.out.println("Rest");
            case CS_DINNER:
                System.out.println("Dinner");
            case CS_TRAIN:
                System.out.println("Train");
            case CS_TEACH:
                System.out.println("Teach");
            default:
                System.out.println("Unknown");
        }
    }
}
