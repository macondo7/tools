package org.tools.tests;

import com.alibaba.fastjson.JSONObject;
import org.tools.vo.MarkOperatorInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EsUdf {
    public void process() {
        Map<String, Object> extMap = new HashMap<>();
        List<MarkOperatorInfo> markOperatorInfo = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            MarkOperatorInfo markOperator = new MarkOperatorInfo();
            markOperator.setMarkOperatorId((long) i + 10000);
            markOperator.setMarkOperatorName("name-" + i);
            markOperator.setMarkTimestamp(System.currentTimeMillis() - i * 11);
            markOperatorInfo.add(markOperator);
        }
        extMap.put("markOperatorInfoList", JSONObject.toJSON(markOperatorInfo));

        // markOperatorInfoList info
        Object markOperatorInfoListObj = extMap.get("markOperatorInfoList");
        if (markOperatorInfoListObj != null && !markOperatorInfoListObj.toString().isEmpty()) {
            System.out.println(markOperatorInfoListObj);
            List<MarkOperatorInfo> markOperatorInfoList = JSONObject.parseArray(markOperatorInfoListObj.toString(), MarkOperatorInfo.class);
            if (markOperatorInfoList != null && !markOperatorInfoList.isEmpty()) {
                MarkOperatorInfo firstMark = markOperatorInfoList.get(0);
                extMap.put("first_mark_cs_id", firstMark.getMarkOperatorId());
                extMap.put("first_mark_cs_name", firstMark.getMarkOperatorName());
            }
        }
        extMap.remove("markOperatorInfoList");
        System.out.println(extMap);
    }

    public static void main(String[] args) {
        EsUdf esUdf = new EsUdf();
        esUdf.process();
    }
}
