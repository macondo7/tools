package org.tools;

import com.alibaba.fastjson2.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.tools.utils.CopyUtil;
import org.tools.vo.CommMark;
import org.tools.vo.NewMark;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class Main {
    public static void main(String[] args) {
        NewMark newMark = new NewMark();
        newMark.setCsId("1");
        newMark.setCsName("name");
        log.debug("newMark: {}", JSON.toJSONString(newMark));
        CommMark commMark = CopyUtil.copy(newMark, CommMark.class);
        log.debug("commMark: {}", JSON.toJSONString(commMark));


        List<Long> list1 = Arrays.asList(1L, 2L, 3L, 4L);
        List<Long> list2 = Arrays.asList(3L, 4L, 5L, 6L);
        Collection<Long> intersection = CollectionUtils.intersection(list1, list2);
        List<Long> intersectionList = new ArrayList<>(intersection);
        System.out.println("Intersection: " + intersectionList);

        List<Long> list3 = list1.stream().filter(x -> x > 10).collect(Collectors.toList());
        System.out.println("List3: " + list3);
        System.out.println("List3: " + null);
    }
}
