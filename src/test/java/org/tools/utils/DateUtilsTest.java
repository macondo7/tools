package org.tools.utils;

import com.google.common.collect.Lists;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DateUtilsTest {

    @Test
    void getLastDayString() {
        String lastDayString = DateUtils.getLastDayString("20210913");
        System.out.println(lastDayString);
        assertEquals("20210912", lastDayString);
        lastDayString = DateUtils.getLastDayString("20240301");
        System.out.println(lastDayString);
        assertEquals("20240229", lastDayString);
        lastDayString = DateUtils.getLastDayString("20240501");
        System.out.println(lastDayString);
    }

    @Test
    void getNextDayStringInBeijing() {
        String nextDayString = DateUtils.getNextDayString("20210913");
        System.out.println(nextDayString);
        assertEquals("20210914", nextDayString);
        nextDayString = DateUtils.getNextDayString("20240228");
        System.out.println(nextDayString);
        assertEquals("20240229", nextDayString);
        nextDayString = DateUtils.getNextDayString("20240229");
        System.out.println(nextDayString);
        assertEquals("20240301", nextDayString);

        List<Long> beforeOrgIdList = Lists.newArrayList(1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L, 9L, 10L);
        // convert beforeOrgIdList to a string, join by ,
        String beforeValue = beforeOrgIdList.stream().map(String::valueOf).reduce((a, b) -> a + "," + b).orElse("");
        System.out.println(beforeValue);
    }
}